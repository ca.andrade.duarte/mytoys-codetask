# MyToys Code Task

This project is developed as a code task for MyToys hiring process.

## Installation

This project is built with [Maven](https://maven.apache.org/) and [Java 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html).
Make sure to have them installed and run the following command:

    mvn install

It's going to download all dependencies, run tests and build the application.

## Running

In order to run the application locally, run:

    mvn spring-boot:run
    
The API documentation will be displayed with an interactive interface at [http://localhost:5000](http://localhost:5000)