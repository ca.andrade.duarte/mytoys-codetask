package mytoys.challenge.service;

import mytoys.challenge.model.Product;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

class ProductCsvReaderTest {

    private ProductCsvReader productCsvReader = new ProductCsvReader();

    @Test
    @DisplayName("Must return a list with the product from the csv")
    public void mustConvertCsv() {
        final String content =  "ID,NAME,PRICE,OLD_PRICE,STOCK,BRAND\n" +
                                "43664,LEGO #14362905,24.99,29.99,0,LEGO";

        MockMultipartFile csvFile = new MockMultipartFile("products", "products.csv", "text/csv", content.getBytes());

        List<Product> products = productCsvReader.readFile(csvFile);

        assertEquals(1, products.size());

        Product product = products.get(0);

        assertEquals(43664, product.getId().intValue());
        assertEquals("LEGO #14362905", product.getName());
        assertEquals(new BigDecimal("24.99"), product.getPrice());
        assertEquals(new BigDecimal("29.99"), product.getOldPrice());
        assertEquals(0, product.getStock().intValue());
        assertEquals("LEGO", product.getBrand());
    }

    @Test
    @DisplayName("Must throw an ResponseStatusException with BAD_REQUEST if file schema is not attended")
    public void mustNotAcceptWrongSchema() {
        final String content =  "NAME,PRICE,OLD_PRICE,STOCK,BRAND\n" +
                                "LEGO #14362905,24.99,29.99,0,LEGO";

        MockMultipartFile csvFile = new MockMultipartFile("products", "products.csv", "text/csv", content.getBytes());

        ResponseStatusException thrown =
                assertThrows(ResponseStatusException.class, () -> productCsvReader.readFile(csvFile));

        assertEquals(HttpStatus.BAD_REQUEST, thrown.getStatus());
    }

    @Test
    @DisplayName("Must throw an ResponseStatusException with BAD_REQUEST if file extension is not csv")
    public void mustNotAcceptNonCsvExtension() {
        MockMultipartFile csvFile = new MockMultipartFile("products", "products.txt", "text/csv", "".getBytes());

        ResponseStatusException thrown =
            assertThrows(ResponseStatusException.class, () -> productCsvReader.validateFile(csvFile));

        assertEquals(HttpStatus.BAD_REQUEST, thrown.getStatus());
    }

    @Test
    @DisplayName("Must throw an ResponseStatusException with BAD_REQUEST if file contentType is not text/csv")
    public void mustNotAcceptNonCsvContentType() {
        MockMultipartFile csvFile = new MockMultipartFile("products", "products.csv", "text/txt", "".getBytes());

        ResponseStatusException thrown =
                assertThrows(ResponseStatusException.class, () -> productCsvReader.validateFile(csvFile));

        assertEquals(HttpStatus.BAD_REQUEST, thrown.getStatus());
    }

}