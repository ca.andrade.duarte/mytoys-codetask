package mytoys.challenge.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.io.FileInputStream;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Must call /products with the csv file for testing and return a valid json with 523 items")
    void mustReadProducts() throws Exception {
        MockMultipartFile csvFile = new MockMultipartFile("csvFile", "products.csv", "text/csv",
                new FileInputStream(getClass().getClassLoader().getResource("20200914_products.csv").getPath()));

        mockMvc.perform(multipart("/product").file(csvFile))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(523)));
    }

    @Test
    @DisplayName("Must call /products with and invalid csv file and get a BAD_REQUEST response")
    void mustNotAcceptInvalidFile() throws Exception {
        MockMultipartFile csvFile = new MockMultipartFile("csvFile", "products.csv", "text/csv",
                new FileInputStream(getClass().getClassLoader().getResource("20200914_invalid_products.csv").getPath()));

        mockMvc.perform(multipart("/product").file(csvFile))
            .andExpect(status().isBadRequest());
    }
}