package mytoys.challenge.service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import mytoys.challenge.model.Product;
import org.apache.commons.io.FilenameUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@Service
public class ProductCsvReader {

    public List<Product> readFile(MultipartFile csvFile) {
        try {
            validateFile(csvFile);

            CsvMapper mapper = new CsvMapper();
            CsvSchema schema = mapper.schemaFor(Product.class).withSkipFirstDataRow(true);

            MappingIterator<Product> iterator = mapper.readerFor(Product.class).with(schema).readValues(csvFile.getBytes());

            return iterator.readAll();
        } catch (IOException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "CSV schema is wrong. Columns: ID,NAME,PRICE,OLD_PRICE,STOCK,BRAND");
        }
    }

    protected void validateFile(MultipartFile csvFile) {
        String extension = FilenameUtils.getExtension(csvFile.getOriginalFilename());
        if (!csvFile.getContentType().equals("text/csv") || !extension.equals("csv"))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "File is not a csv.");
    }

}
