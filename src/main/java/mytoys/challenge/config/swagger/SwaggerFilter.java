package mytoys.challenge.config.swagger;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class SwaggerFilter extends GenericFilter {

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest)servletRequest;
    HttpServletResponse response = (HttpServletResponse)servletResponse;
    if (request.getRequestURI().equals("") || request.getRequestURI().equals("/")) {
      response.sendRedirect("/swagger-ui/");
      return;
    }
    chain.doFilter(request, response);
  }
}
