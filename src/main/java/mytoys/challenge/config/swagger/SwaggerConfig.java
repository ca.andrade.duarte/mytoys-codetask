package mytoys.challenge.config.swagger;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@Import(SpringDataRestConfiguration.class)
class SwaggerConfig {

  @Bean
  public FilterRegistrationBean<SwaggerFilter> filter(SwaggerFilter swaggerFilter) {
    FilterRegistrationBean<SwaggerFilter> filterRegistration = new FilterRegistrationBean<>(swaggerFilter);
    filterRegistration.setOrder(-200);
    return filterRegistration;
  }

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
      .select()
      .apis(RequestHandlerSelectors.any())
      .paths(PathSelectors.ant("/error/**").negate())
      .build();
  }

}
