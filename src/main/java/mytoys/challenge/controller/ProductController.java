package mytoys.challenge.controller;

import mytoys.challenge.model.Product;
import mytoys.challenge.service.ProductCsvReader;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductCsvReader productCsvReader;

    public ProductController(ProductCsvReader productCsvReader) {
        this.productCsvReader = productCsvReader;
    }

    @PostMapping
    public ResponseEntity<List<Product>> readProducts(@RequestPart(required = true) MultipartFile csvFile) {
        return ResponseEntity.ok(productCsvReader.readFile(csvFile));
    }

}
